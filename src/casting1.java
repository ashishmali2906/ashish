public class casting1 {
    public static void main(String[] args) {
        char ch1 = 'A';
        char ch2 = 'd';
        int a = ch1;
        int b = ch2;
        System.out.println(ch1 + "\t" + ch2);
        System.out.println("");

        int k= (int) 10.5;
        double l=10;
        {
            System.out.println(k + "\t" + l);
        }
    }
}
